Statistic scripts for GitLab.

Default License is Apache 2.0.

[![Build Status](https://gitlab.com/mfriedenhagen/gitlabstats/badges/master/build.svg)](https://gitlab.com/mfriedenhagen/gitlabstats/commits/master)
