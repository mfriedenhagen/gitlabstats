package de.friedenhagen.gitlabstats

import org.gitlab.api.GitlabAPI
import org.gitlab.api.models.GitlabBuild
import org.gitlab.api.models.GitlabProject
import java.io.Serializable

interface GitlabAPIAdapter {
    fun getBuilds(gitlabProject : Serializable) : List<MyGitlabBuild>
    fun getProject(gitlabProject : Serializable) : GitlabProject
}

class GitlabAPIAdapterImpl(private val api : GitlabAPI) : GitlabAPIAdapter {
    override fun getProject(gitlabProject: Serializable): GitlabProject {
        return api.getProject(gitlabProject)
    }

    override fun getBuilds(gitlabProject: Serializable): List<MyGitlabBuild> {
        val project = api.getProject(gitlabProject)
        val tailUrl = GitlabProject.URL + "/" + project.id + GitlabBuild.URL;
        return api.retrieve().getAll(tailUrl, Array<MyGitlabBuild>::class.java)
    }

}