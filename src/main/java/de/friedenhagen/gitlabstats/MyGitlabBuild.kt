package de.friedenhagen.gitlabstats

import com.fasterxml.jackson.annotation.JsonProperty
import org.gitlab.api.models.GitlabBuild

/**
 * Needs to be overridden until https://github.com/timols/java-gitlab-api/pull/144 is merged and released.
 */
class MyGitlabBuild : GitlabBuild() {
    @JsonProperty("finished_at")
    private var finishedAt: String? = null
    override fun getFinishedAt() : String? {
        return finishedAt
    }
    override fun setFinishedAt(value: String?) {
        finishedAt = value
    }
}