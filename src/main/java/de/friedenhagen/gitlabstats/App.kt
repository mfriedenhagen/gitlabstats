package de.friedenhagen.gitlabstats

import org.apache.commons.csv.CSVFormat
import org.gitlab.api.GitlabAPI
import java.io.PrintStream
import java.net.URI

class App(private val apiAdapter: GitlabAPIAdapter, private val out : PrintStream = System.out) {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val gitlabToken: String = System.getenv("GITLAB_TOKEN")
            val gitlabURI = URI(System.getenv("GITLAB_URL"))
            val gitlabProject = System.getenv("GITLAB_PROJECT")
            val api = GitlabAPI.connect(gitlabURI.toString(), gitlabToken)
            val app = App(GitlabAPIAdapterImpl(api))
            app.showBuilds(gitlabProject)
        }
    }

    fun showBuilds(gitlabProject: String) {
        CSVFormat.DEFAULT.withHeader(
                "project",
                "id",
                "stage",
                "branch",
                "name",
                "runner",
                "status",
                "startedAt",
                "createdAt",
                "finishedAt"
                ).print(out)
        val project = apiAdapter.getProject(gitlabProject)
        apiAdapter.getBuilds(gitlabProject).forEach {
            CSVFormat.DEFAULT.printRecord(
                    out,
                    gitlabProject,
                    "${project.webUrl}/builds/${it.id}",
                    it.stage,
                    it.ref,
                    it.name,
                    it.runner?.description ?: "UNKNOWN",
                    it.status,
                    it.startedAt,
                    it.createdAt,
                    it.finishedAt)
        }
    }
}

