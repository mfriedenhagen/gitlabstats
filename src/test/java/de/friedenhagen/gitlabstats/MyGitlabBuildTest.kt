package de.friedenhagen.gitlabstats

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import io.kotlintest.specs.FunSpec

class MyGitlabBuildTest : FunSpec() {

    init {
        val sut = listOfMyGitlabBuildsFromFile()
        test("Builds should have 20 entries") {
            sut.size shouldEqual 20
        }

        test("First build should have status success") {
            sut[0].status shouldEqual "success"
        }
    }

    companion object {
        @JvmStatic
        val MAPPER = ObjectMapper()

        init {
            MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        }
        internal fun listOfMyGitlabBuildsFromFile(): List<MyGitlabBuild> {
            return  MAPPER.readValue(
                    MyGitlabBuildTest::class.java.getResourceAsStream(
                            "/builds.json"), Array<MyGitlabBuild>::class.java).asList()
        }

    }
}