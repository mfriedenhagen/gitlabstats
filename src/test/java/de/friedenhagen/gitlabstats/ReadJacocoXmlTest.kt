package de.friedenhagen.gitlabstats

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule
import com.fasterxml.jackson.dataformat.xml.XmlFactory
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import io.kotlintest.specs.FunSpec
import javax.xml.stream.XMLInputFactory
data class JacocoReport(var counter : List<Counter> = emptyList())

data class Counter(var type: CounterType = CounterType.BRANCH, var missed : Int = 0, var covered: Int = 0) {
    val total : Int get() = missed + covered
    val relative: Double get() = 1.0 * covered / total
}

enum class CounterType {
    INSTRUCTION, BRANCH, LINE, COMPLEXITY, METHOD, CLASS
}

class ReadJacocoXmlTest : FunSpec() {
    init {
        test("Read jacoco.xml") {
            val xmlInputFactory = XMLInputFactory.newInstance()
            xmlInputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false)
            val factory = XmlFactory(xmlInputFactory)
            val xmlModule = JacksonXmlModule()
            xmlModule.setDefaultUseWrapper(false)
            val mapper = XmlMapper(factory, xmlModule)
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

            val report = ReadJacocoXmlTest::class.java.getResourceAsStream("/jacoco.xml").reader(charset("UTF-8")).use {
                mapper.readValue(it, JacocoReport::class.java)
            }
            println(report)
        }
    }
}