package de.friedenhagen.gitlabstats

import io.kotlintest.mock.`when`
import io.kotlintest.mock.mock
import io.kotlintest.specs.FunSpec
import org.gitlab.api.models.GitlabProject
import java.io.ByteArrayOutputStream
import java.io.PrintStream

class AppTest : FunSpec() {
    init {
        test("CSV should have 21 lines") {
            // given
            val apiAdapterMock = mock<GitlabAPIAdapter>()
            val project = GitlabProject()
            project.id = 5
            project.webUrl = "https://example.com/project"
            `when`(apiAdapterMock.getProject("example")).thenReturn(project, project)
            `when`(apiAdapterMock.getBuilds("example")).thenReturn(MyGitlabBuildTest.listOfMyGitlabBuildsFromFile())
            // when
            val out = ByteArrayOutputStream()
            val sut = App(apiAdapterMock, PrintStream(out))
            sut.showBuilds("example")
            // then
            val result = out.toByteArray().filter{it.toChar() == '\n'}.size
            result shouldEqual 21
        }
        test("Empty input should have 1 line") {
            // given
            val apiAdapterMock = mock<GitlabAPIAdapter>()
            val project = GitlabProject()
            project.id = 5
            project.webUrl = "https://example.com/project"
            `when`(apiAdapterMock.getProject("example")).thenReturn(project, project)
            `when`(apiAdapterMock.getBuilds("example")).thenReturn(emptyList())
            // when
            val out = ByteArrayOutputStream()
            val sut = App(apiAdapterMock, PrintStream(out))
            sut.showBuilds("example")
            // then
            val result = out.toByteArray().filter{it.toChar() == '\n'}.size
            result shouldEqual 1
        }
    }
}