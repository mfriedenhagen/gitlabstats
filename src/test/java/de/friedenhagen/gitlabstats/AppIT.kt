package de.friedenhagen.gitlabstats

import java.io.ByteArrayOutputStream
import java.io.PrintStream
import io.kotlintest.specs.FunSpec

class AppIT : FunSpec() {
    val originalOut = System.out
    val out = ByteArrayOutputStream()

    init {
        test("Should return values") {
            App.main(emptyArray())
            val result = out.toByteArray()
            assert(result.size > 0)
        }.config(ignored = (System.getenv("GITLAB_TOKEN") == null))
    }

    override fun beforeEach() {
        System.setOut(PrintStream(out))
    }

    override fun afterEach() {
        System.setOut(originalOut)
    }
}