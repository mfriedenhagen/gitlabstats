package de.friedenhagen.gitlabstats

import io.kotlintest.specs.FunSpec
import org.yaml.snakeyaml.Yaml
import java.io.File
import java.util.*

class ReadGitlabCiYamlTest : FunSpec() {
    init {
        test("Should read gitlab-ci.yml") {
            val yaml = Yaml()
            val reader = File(".gitlab-ci.yml").reader(charset("UTF-8"))
            val any = yaml.load(reader)
            if (any is LinkedHashMap<*,*>) {
                any.forEach {
                    println(it.key)
                    println(it.value)
                }
            }
            any.javaClass shouldEqual LinkedHashMap::class.java
            println(any.javaClass)
        }
    }
}